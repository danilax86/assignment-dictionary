%define SYS_READ 0
%define SYS_WRITE 1
%define STDIN 0
%define STDOUT 1
%define EXIT 60

section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rax + rdi], 0
        je .exit
        inc rax
        jmp .loop
    .exit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length  ; Вычисляем длину строки и сохраняем в rax
    pop rdi
    mov rsi, rdi        ; Записываем адрес строки
    mov rdx, rax        ; Записываем длину строки
    mov rax, SYS_WRITE  ; Системный вызов sys_write
    mov rdi, STDOUT     ; Стандартный вывод
    syscall
    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; Кладём на стек аргумент
    mov rdi, rsp        ; Сохраняем адрес вершины стека
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi        ; Записываем полученное число
    mov rdi, 10         ; Запоминаем основание СС для деления
    push 0x00           ; Нуль-терминатор

    .loop:
        xor rdx, rdx    ; Избавляемся от остатка на каждой итерации
        div rdi         ; Делим на 10, остаток помещается в rdx
        add rdx, '0'    ; Переводим в ASCII
        push rdx        ; Запоминаем число в стеке
        test rax, rax   ; Если в rax пусто,  `test` установит ZF в 0 =>
        jz .write       ; прыгаем на печать числа,
        jmp .loop       ; иначе проходим по числу дальше

    .write:
        pop rdi
        cmp rdi, 0x00   ; Если нуль-терминатор
        je .exit        ; выходим,
        call print_char ; иначе выводим цифру
        jmp .write      ; Заходим в цикл

    .exit:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi       ; Если число положительное,
    jns print_uint      ; печатаем его,
    push rdi            ; иначе запоминаем число в стеке.
    mov rdi, '-'        ; Помещаем `-`
    call print_char     ; Печатаем `-`
    pop rdi             ; Достаём число из стека
    neg rdi             ; Инвертируем + 1 => получаем положительное число
    jmp print_uint      ; Печатаем число

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    push r8
    push r9
    xor r8, r8
    xor r9, r9

    .loop:
        mov r8b, [rdi + rcx]
        mov r9b, [rsi + rcx]
        cmp r8b, r9b
        jne .not_equals
        test r8b, r9b
        jz .equals
        inc rcx
        jmp .loop

    .equals:
        mov rax, 1
        pop r9
        pop r8
        ret

    .not_equals:
        xor rax, rax
        pop r9
        pop r8
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYS_READ    ; rax = 0 sys_read
    mov rdi, STDIN      ; rdi = 0 stdin
    mov rdx, 1          ; Количество символов
    mov rsi, rsp        ; Указатель на начало и конец (один символ ведь)
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx                        ; callee-saved регистр
    push r8
    push r9
    mov r8, rsi                     ; размер буфера
    mov r9, rdi                     ; адрес буфера
    xor rbx, rbx                    ; счётчик длины

    .skip:                          ; Начальная проверка на пробелы
        call read_char
        test rax, rax               ; Если нуль-терминатор --
        jz .success                  ; завершаем, иначе
        mov byte[r9 + rbx], al      ; записываем в буфер
        cmp byte[r9 + rbx], 0x20    ; Проверяем на пробелы
        je .skip
        cmp byte[r9 + rbx], 0x9     ; Проверяем на табуляцию
        je .skip
        cmp byte[r9 + rbx], 0xA     ; Проверяем на символ перевода строки
        je .skip
        inc rbx                     ; Идём читать следующий символ (двигаем указатель)

    .loop:
        call read_char
        cmp al, 0x21                ; Проверяем на пробельные символы
        jl .success
        mov byte[r9 + rbx], al      ; Пишем символ в буфер
        cmp r8, rbx                 ; Хватит ли места в буфере на следующий символ?
        jl .fail
        inc rbx                     ; Идём читать следующий символ
        jmp .loop

    .success:
        mov rdx, rbx                ; Длина строки
        mov rax, r9                 ; Адрес буфера
        mov byte[r9 + rbx], 0       ; Дописываем нуль-терминатор
        pop r9
        pop r8
        pop rbx
        ret

    .fail:
        xor rax, rax                ; Неудача = 0 в rax
        pop r9
        pop r8
        pop rbx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
		                            ; rdi -- указатель на строку.
	xor rax, rax		            ; Тут будем хранить число.
	push r8                         ; Тут будем хранить символ
	push r9                         ; Тут будем хранить систему счисления
	push r10                        ; Тут будем хранить длину строки
	mov r9, 10                      ; Система счисления
	xor r10, r10                    ; Длина строки

    .loop:
        xor r8, r8
        cmp byte[rdi + r10], '0'    ; Если код символа меньше нуля -- не цифра
        jb .exit
        cmp byte[rdi + r10], '9'    ; Если код символа больше девяти -- не цифра
        ja .exit
        mov r8b, byte [rdi+r10]     ; Запоминаем символ
        sub r8b, '0'                ; Переводим символ в цифру
        imul rax, r9
        add rax, r8                 ; Записываем полученную цифру в аккумулятор
        inc r10                     ; Переходим на следующий символ
        jmp .loop

    .exit:
        mov rdx, r10
        pop r10
        pop r9
        pop r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    jmp parse_uint                  ; Если беззнаковое, то парсим как обычно

    .negative:
        inc rdi                     ; Если знаковое, пропускаем знак (тем самым уменьшая длину)
        call parse_uint             ; Парсим число без '-' (получаем в rax число, в rdx -- длину),
        inc rdx                     ; затем прибавляем к длине числа 1, так как у нас ещё есть '-' (возвращаем длину)
        neg rax                     ; Инверсия + 1
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        je .fail
        mov rcx, [rdi + rax]
        mov [rsi + rax], rcx
        test rcx, rcx
        jz .exit
        inc rax
        jmp .loop

    .fail:
        xor rax, rax

    .exit:
        ret