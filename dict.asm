section .text
extern string_equals
global find_word


; В файле dict.asm создать функцию find_word. Она принимает два аргумента:

; Указатель на нуль-терминированную строку. (rdi)
; Указатель на начало словаря. (rsi)

; find_word пройдёт по всему словарю в поисках подходящего ключа. 
; Если подходящее вхождение найдено, вернёт адрес начала вхождения всловарь (не значения), 
; иначе вернёт 0.
find_word:
        push rsi
    	push rdi
	add rsi, 8
	call string_equals
	pop rdi
	pop rsi
	test rax, rax
	jnz .success
	mov rsi, [rsi]
	test rsi, rsi
        jz .fail
        jmp find_word
		
    .success:
	mov rax, rsi
        ret
    .fail:
	mov rax, 0
        ret
