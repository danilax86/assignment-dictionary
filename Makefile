AS=nasm
ASFLAGS=-f elf64 -o

.PHONY: lab2 clear

lab2: main.o lib.o dict.o
	ld main.o lib.o dict.o -o lab2
%.o: %.asm
	$(AS) $(ASFLAGS) $@ $<

clear:
	rm -rf *.o lab2

