%include "colon.inc"
%include "words.inc"
extern exit
extern print_string
extern string_length
extern print_char
extern print_newline
extern print_uint
extern print_int
extern string_equals
extern read_char
extern read_word
extern parse_uint
extern parse_int
extern string_copy
extern find_word
global _start

%define SYS_WRITE 1
%define STDERR 2
%define limit 255
%define rbytes 8

section .data
    input_message: db "Input your key: ", 0
    buffer: times 255 db 0
    key_error: db "Invalid key", 0xA, 0
    key_len_error: db "Invalid key length", 0xA, 0

section .text

print_err:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDERR
    syscall
    xor rax, rax
    ret

_start:
    mov rdi, input_message
    call print_string
    mov rdi, buffer
    mov rsi, limit
    call read_word
    test rax, rax
    jz .print_key_len_error
    mov rdi, rax
    mov rsi, ptr
    call find_word
    test rax, rax
    jz .print_key_error
    add rax, rbytes
    mov rdi, rax
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call exit

    .print_key_len_error:
        mov rdi, key_len_error
        call print_err
        call exit

    .print_key_error:
        mov rdi, key_error
        call print_err
        call exit
